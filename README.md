# Monitaur Frontend Challenge
The challenge, should you choose to accept, is meant to assess your overall competency with HTML, CSS, and JavaScript.

## Challenge
The following is a simple Figma mockup composing of HTML tables, navigation, and buttons. 
https://www.figma.com/proto/aqW9rOsDDkJpZi3YmFFTkh/Untitled?node-id=2%3A686&scaling=min-zoom&page-id=0%3A1

To test the extent of your experience with HTML, CSS, and JavaScript, please create a new project (Vue is preferred) to implement the Figma mockup above.

## Time limit
You should spend no more than 4 hours on this excersice. The objective is not to complete it 100%. In fact, we want to see an unfinished project so we can assess the trade-offs that you made.

## Submission
Create a merge request into this repository and request for code review from @aamustapha and @michaelherman.
